{
    "header": {
        "pipelineVersion": "2.2",
        "releaseVersion": "2021.1.0",
        "fileVersion": "1.1",
        "nodesVersions": {
            "FeatureExtraction": "1.1",
            "DepthMapFilter": "3.0",
            "FeatureMatching": "2.0",
            "DepthMap": "2.0",
            "Texturing": "5.0",
            "StructureFromMotion": "2.0",
            "CameraInit": "4.0",
            "Meshing": "7.0",
            "PrepareDenseScene": "3.0",
            "MeshFiltering": "3.0",
            "ImageMatching": "2.0"
        }
    },
    "graph": {
        "CameraInit_1": {
            "nodeType": "CameraInit",
            "position": [
                0,
                0
            ],
            "parallelization": {
                "blockSize": 0,
                "size": 30,
                "split": 1
            },
            "uids": {
                "0": "cf2eb97f6c534ccda2d67e63cf36042f48effba9"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "viewpoints": [
                    {
                        "viewId": 4941168,
                        "poseId": 4941168,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115559.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:56:00\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-0.65\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:56:00\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:56:00\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"680\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"219179\", \"Exif:SubsecTimeDigitized\": \"219179\", \"Exif:SubsecTimeOriginal\": \"219179\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 40953125,
                        "poseId": 40953125,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115700.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:57:01\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-1.81\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:57:01\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:57:01\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"1264\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"836698\", \"Exif:SubsecTimeDigitized\": \"836698\", \"Exif:SubsecTimeOriginal\": \"836698\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 52212805,
                        "poseId": 52212805,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115711.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:57:12\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-0.66\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:57:12\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:57:12\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"194\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"030950\", \"Exif:SubsecTimeDigitized\": \"030950\", \"Exif:SubsecTimeOriginal\": \"030950\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 132826813,
                        "poseId": 132826813,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115657.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:56:58\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-0.71\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:56:58\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:56:58\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"710\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"637416\", \"Exif:SubsecTimeDigitized\": \"637416\", \"Exif:SubsecTimeOriginal\": \"637416\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 293773218,
                        "poseId": 293773218,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115708.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:57:09\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-0.02\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:57:09\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:57:09\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"102\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"729034\", \"Exif:SubsecTimeDigitized\": \"729034\", \"Exif:SubsecTimeOriginal\": \"729034\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 465707962,
                        "poseId": 465707962,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115639.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:56:40\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-1.31\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:56:40\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:56:40\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"297\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"317633\", \"Exif:SubsecTimeDigitized\": \"317633\", \"Exif:SubsecTimeOriginal\": \"317633\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 476654160,
                        "poseId": 476654160,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115713.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:57:14\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-1.66\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:57:14\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:57:14\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"641\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"307143\", \"Exif:SubsecTimeDigitized\": \"307143\", \"Exif:SubsecTimeOriginal\": \"307143\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 478357782,
                        "poseId": 478357782,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115728.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:57:29\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-1.3\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:57:29\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:57:29\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"641\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"649035\", \"Exif:SubsecTimeDigitized\": \"649035\", \"Exif:SubsecTimeOriginal\": \"649035\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 711092900,
                        "poseId": 711092900,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115549.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:55:50\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-0.65\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:55:50\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:55:50\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"670\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"227847\", \"Exif:SubsecTimeDigitized\": \"227847\", \"Exif:SubsecTimeOriginal\": \"227847\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 789363665,
                        "poseId": 789363665,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115622.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:56:23\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-0.72\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:56:23\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:56:23\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"405\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"019659\", \"Exif:SubsecTimeDigitized\": \"019659\", \"Exif:SubsecTimeOriginal\": \"019659\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 825394917,
                        "poseId": 825394917,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115720.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:57:21\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-1.05\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:57:21\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:57:21\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"424\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"232915\", \"Exif:SubsecTimeDigitized\": \"232915\", \"Exif:SubsecTimeOriginal\": \"232915\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 949858249,
                        "poseId": 949858249,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115705.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:57:06\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-0.36\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:57:06\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:57:06\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"153\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"547594\", \"Exif:SubsecTimeDigitized\": \"547594\", \"Exif:SubsecTimeOriginal\": \"547594\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 962937498,
                        "poseId": 962937498,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115652.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:56:53\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-1.29\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:56:53\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:56:53\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"927\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"766500\", \"Exif:SubsecTimeDigitized\": \"766500\", \"Exif:SubsecTimeOriginal\": \"766500\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 977729866,
                        "poseId": 977729866,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115608.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:56:08\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-0.05\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:56:08\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:56:08\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"115\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"947941\", \"Exif:SubsecTimeDigitized\": \"947941\", \"Exif:SubsecTimeOriginal\": \"947941\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 997824443,
                        "poseId": 997824443,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115611.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:56:12\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-0.1\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:56:12\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:56:12\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"149\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"519138\", \"Exif:SubsecTimeDigitized\": \"519138\", \"Exif:SubsecTimeOriginal\": \"519138\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1048082686,
                        "poseId": 1048082686,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115618.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:56:19\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-1.37\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:56:19\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:56:19\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"537\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"407746\", \"Exif:SubsecTimeDigitized\": \"407746\", \"Exif:SubsecTimeOriginal\": \"407746\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1269600366,
                        "poseId": 1269600366,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115631.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:56:32\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-0.66\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:56:32\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:56:32\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"202\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"470162\", \"Exif:SubsecTimeDigitized\": \"470162\", \"Exif:SubsecTimeOriginal\": \"470162\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1367924898,
                        "poseId": 1367924898,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115723.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:57:24\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-1.05\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:57:24\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:57:24\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"284\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"294156\", \"Exif:SubsecTimeDigitized\": \"294156\", \"Exif:SubsecTimeOriginal\": \"294156\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1456661526,
                        "poseId": 1456661526,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115615.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:56:16\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-0.69\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:56:16\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:56:16\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"238\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"061607\", \"Exif:SubsecTimeDigitized\": \"061607\", \"Exif:SubsecTimeOriginal\": \"061607\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1517672580,
                        "poseId": 1517672580,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115540.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:55:41\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-1.02\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:55:41\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:55:41\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"249\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"473360\", \"Exif:SubsecTimeDigitized\": \"473360\", \"Exif:SubsecTimeOriginal\": \"473360\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1518618607,
                        "poseId": 1518618607,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115635.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:56:36\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-0.69\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:56:36\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:56:36\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"238\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"833615\", \"Exif:SubsecTimeDigitized\": \"833615\", \"Exif:SubsecTimeOriginal\": \"833615\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1608593203,
                        "poseId": 1608593203,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115717.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:57:18\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-1.35\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:57:18\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:57:18\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"824\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"389447\", \"Exif:SubsecTimeDigitized\": \"389447\", \"Exif:SubsecTimeOriginal\": \"389447\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1642758074,
                        "poseId": 1642758074,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115626.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:56:27\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-0.69\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:56:27\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:56:27\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"272\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"630205\", \"Exif:SubsecTimeDigitized\": \"630205\", \"Exif:SubsecTimeOriginal\": \"630205\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1650377554,
                        "poseId": 1650377554,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115725.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:57:26\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-1.33\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:57:26\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:57:26\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"463\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"834894\", \"Exif:SubsecTimeDigitized\": \"834894\", \"Exif:SubsecTimeOriginal\": \"834894\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1686200122,
                        "poseId": 1686200122,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115604.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:56:05\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-0.45\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:56:05\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:56:05\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"152\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"4.906\", \"Exif:SubsecTime\": \"242201\", \"Exif:SubsecTimeDigitized\": \"242201\", \"Exif:SubsecTimeOriginal\": \"242201\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.0333333\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1710437144,
                        "poseId": 1710437144,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115655.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:56:56\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-1.02\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:56:56\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:56:56\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"743\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"285144\", \"Exif:SubsecTimeDigitized\": \"285144\", \"Exif:SubsecTimeOriginal\": \"285144\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1712011535,
                        "poseId": 1712011535,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115543.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:55:44\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-1.02\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:55:44\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:55:44\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"537\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"231570\", \"Exif:SubsecTimeDigitized\": \"231570\", \"Exif:SubsecTimeOriginal\": \"231570\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1976046802,
                        "poseId": 1976046802,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115649.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:56:50\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-1.66\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:56:50\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:56:50\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"561\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"863224\", \"Exif:SubsecTimeDigitized\": \"863224\", \"Exif:SubsecTimeOriginal\": \"863224\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 2053359534,
                        "poseId": 2053359534,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115703.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:57:04\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-1.05\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:57:04\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:57:04\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"743\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"050070\", \"Exif:SubsecTimeDigitized\": \"050070\", \"Exif:SubsecTimeOriginal\": \"050070\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 2060494919,
                        "poseId": 2060494919,
                        "path": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New/IMG_20230116_115554.jpg",
                        "intrinsicId": 1684837985,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:SensorWidthEstimation\": \"8.570345\", \"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:16 11:55:55\", \"Exif:ApertureValue\": \"1.83\", \"Exif:BrightnessValue\": \"-1.3\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:16 11:55:55\", \"Exif:DateTimeOriginal\": \"2023:01:16 11:55:55\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"2\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.43\", \"Exif:FocalLengthIn35mmFilm\": \"25\", \"Exif:LightSource\": \"21\", \"Exif:MaxApertureValue\": \"1.83\", \"Exif:MeteringMode\": \"2\", \"Exif:PhotographicSensitivity\": \"1028\", \"Exif:PixelXDimension\": \"4624\", \"Exif:PixelYDimension\": \"2080\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SensingMethod\": \"1\", \"Exif:ShutterSpeedValue\": \"5.058\", \"Exif:SubsecTime\": \"547852\", \"Exif:SubsecTimeDigitized\": \"547852\", \"Exif:SubsecTimeOriginal\": \"547852\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.030303\", \"FNumber\": \"1.89\", \"Make\": \"Xiaomi\", \"Model\": \"M2007J20CG\", \"Orientation\": \"6\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    }
                ],
                "intrinsics": [
                    {
                        "intrinsicId": 1684837985,
                        "pxInitialFocalLength": 2929.674291280765,
                        "pxFocalLength": 2929.674291280765,
                        "type": "radial3",
                        "width": 4624,
                        "height": 2080,
                        "sensorWidth": 8.5703452000541,
                        "sensorHeight": 3.855172581339214,
                        "serialNumber": "C:/Users/Darky/Desktop/Photogrammetry/Photo/New_Xiaomi_M2007J20CG",
                        "principalPoint": {
                            "x": 2312.0,
                            "y": 1040.0
                        },
                        "initializationMode": "estimated",
                        "distortionParams": [
                            0.0,
                            0.0,
                            0.0
                        ],
                        "locked": false
                    }
                ],
                "sensorDatabase": "D:\\Meshroom-2021.1.0\\aliceVision\\share\\aliceVision\\cameraSensors.db",
                "defaultFieldOfView": 45.0,
                "groupCameraFallback": "folder",
                "allowedCameraModels": [
                    "pinhole",
                    "radial1",
                    "radial3",
                    "brown",
                    "fisheye4",
                    "fisheye1"
                ],
                "useInternalWhiteBalance": true,
                "viewIdMethod": "metadata",
                "viewIdRegex": ".*?(\\d+)",
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/cameraInit.sfm"
            }
        },
        "FeatureExtraction_1": {
            "nodeType": "FeatureExtraction",
            "position": [
                200,
                0
            ],
            "parallelization": {
                "blockSize": 40,
                "size": 30,
                "split": 1
            },
            "uids": {
                "0": "57a9fefcee826189f0e2c46f17c01d7d27e601d4"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{CameraInit_1.output}",
                "describerTypes": [
                    "sift"
                ],
                "describerPreset": "normal",
                "maxNbFeatures": 0,
                "describerQuality": "normal",
                "contrastFiltering": "GridSort",
                "relativePeakThreshold": 0.01,
                "gridFiltering": true,
                "forceCpuExtraction": true,
                "maxThreads": 0,
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/"
            }
        },
        "ImageMatching_1": {
            "nodeType": "ImageMatching",
            "position": [
                400,
                0
            ],
            "parallelization": {
                "blockSize": 0,
                "size": 30,
                "split": 1
            },
            "uids": {
                "0": "19a07db9c01998c9967fb9a1c23d5ab4bbdb7263"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{FeatureExtraction_1.input}",
                "featuresFolders": [
                    "{FeatureExtraction_1.output}"
                ],
                "method": "VocabularyTree",
                "tree": "D:\\Meshroom-2021.1.0\\aliceVision\\share\\aliceVision\\vlfeat_K80L3.SIFT.tree",
                "weights": "",
                "minNbImages": 200,
                "maxDescriptors": 500,
                "nbMatches": 50,
                "nbNeighbors": 50,
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/imageMatches.txt"
            }
        },
        "FeatureMatching_1": {
            "nodeType": "FeatureMatching",
            "position": [
                600,
                0
            ],
            "parallelization": {
                "blockSize": 20,
                "size": 30,
                "split": 2
            },
            "uids": {
                "0": "66875801f0aca1409e13101c61615621383e5ef8"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{ImageMatching_1.input}",
                "featuresFolders": "{ImageMatching_1.featuresFolders}",
                "imagePairsList": "{ImageMatching_1.output}",
                "describerTypes": "{FeatureExtraction_1.describerTypes}",
                "photometricMatchingMethod": "ANN_L2",
                "geometricEstimator": "acransac",
                "geometricFilterType": "fundamental_matrix",
                "distanceRatio": 0.8,
                "maxIteration": 2048,
                "geometricError": 0.0,
                "knownPosesGeometricErrorMax": 5.0,
                "maxMatches": 0,
                "savePutativeMatches": false,
                "crossMatching": false,
                "guidedMatching": false,
                "matchFromKnownCameraPoses": false,
                "exportDebugFiles": false,
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/"
            }
        },
        "StructureFromMotion_1": {
            "nodeType": "StructureFromMotion",
            "position": [
                800,
                0
            ],
            "parallelization": {
                "blockSize": 0,
                "size": 30,
                "split": 1
            },
            "uids": {
                "0": "1a9e99b9902641dd88b8e6c18543ae6c5b32d8ab"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{FeatureMatching_1.input}",
                "featuresFolders": "{FeatureMatching_1.featuresFolders}",
                "matchesFolders": [
                    "{FeatureMatching_1.output}"
                ],
                "describerTypes": "{FeatureMatching_1.describerTypes}",
                "localizerEstimator": "acransac",
                "observationConstraint": "Basic",
                "localizerEstimatorMaxIterations": 4096,
                "localizerEstimatorError": 0.0,
                "lockScenePreviouslyReconstructed": false,
                "useLocalBA": true,
                "localBAGraphDistance": 1,
                "maxNumberOfMatches": 0,
                "minNumberOfMatches": 0,
                "minInputTrackLength": 2,
                "minNumberOfObservationsForTriangulation": 2,
                "minAngleForTriangulation": 3.0,
                "minAngleForLandmark": 2.0,
                "maxReprojectionError": 4.0,
                "minAngleInitialPair": 5.0,
                "maxAngleInitialPair": 40.0,
                "useOnlyMatchesFromInputFolder": false,
                "useRigConstraint": true,
                "lockAllIntrinsics": false,
                "filterTrackForks": false,
                "initialPairA": "",
                "initialPairB": "",
                "interFileExtension": ".abc",
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/sfm.abc",
                "outputViewsAndPoses": "{cache}/{nodeType}/{uid0}/cameras.sfm",
                "extraInfoFolder": "{cache}/{nodeType}/{uid0}/"
            }
        },
        "PrepareDenseScene_1": {
            "nodeType": "PrepareDenseScene",
            "position": [
                1000,
                0
            ],
            "parallelization": {
                "blockSize": 40,
                "size": 30,
                "split": 1
            },
            "uids": {
                "0": "c774f07f480a1e20fd5d0c0514ac35db912a8a6f"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{StructureFromMotion_1.output}",
                "imagesFolders": [],
                "outputFileType": "exr",
                "saveMetadata": true,
                "saveMatricesTxtFiles": false,
                "evCorrection": false,
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/",
                "outputUndistorted": "{cache}/{nodeType}/{uid0}/*.{outputFileTypeValue}"
            }
        },
        "DepthMap_1": {
            "nodeType": "DepthMap",
            "position": [
                1200,
                0
            ],
            "parallelization": {
                "blockSize": 3,
                "size": 30,
                "split": 10
            },
            "uids": {
                "0": "0a9a12643cb2ce348d0e0b41bc5aa575c2f98c7e"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{PrepareDenseScene_1.input}",
                "imagesFolder": "{PrepareDenseScene_1.output}",
                "downscale": 4,
                "minViewAngle": 2.0,
                "maxViewAngle": 70.0,
                "sgmMaxTCams": 10,
                "sgmWSH": 4,
                "sgmGammaC": 5.5,
                "sgmGammaP": 8.0,
                "refineMaxTCams": 6,
                "refineNSamplesHalf": 150,
                "refineNDepthsToRefine": 31,
                "refineNiters": 100,
                "refineWSH": 3,
                "refineSigma": 15,
                "refineGammaC": 15.5,
                "refineGammaP": 8.0,
                "refineUseTcOrRcPixSize": false,
                "exportIntermediateResults": false,
                "nbGPUs": 0,
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/"
            }
        },
        "DepthMapFilter_1": {
            "nodeType": "DepthMapFilter",
            "position": [
                1400,
                0
            ],
            "parallelization": {
                "blockSize": 10,
                "size": 30,
                "split": 3
            },
            "uids": {
                "0": "7682b0dc0a742246b8ea0afc1fddc325ba08b061"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{DepthMap_1.input}",
                "depthMapsFolder": "{DepthMap_1.output}",
                "minViewAngle": 2.0,
                "maxViewAngle": 70.0,
                "nNearestCams": 10,
                "minNumOfConsistentCams": 3,
                "minNumOfConsistentCamsWithLowSimilarity": 4,
                "pixSizeBall": 0,
                "pixSizeBallWithLowSimilarity": 0,
                "computeNormalMaps": false,
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/"
            }
        },
        "Meshing_1": {
            "nodeType": "Meshing",
            "position": [
                1600,
                0
            ],
            "parallelization": {
                "blockSize": 0,
                "size": 1,
                "split": 1
            },
            "uids": {
                "0": "c18d29971c54fb72e2920777b61e22134acacf5c"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{DepthMapFilter_1.input}",
                "depthMapsFolder": "{DepthMapFilter_1.output}",
                "useBoundingBox": true,
                "boundingBox": {
                    "bboxTranslation": {
                        "x": 0.0,
                        "y": 0.0,
                        "z": 2.275261878967285
                    },
                    "bboxRotation": {
                        "x": 0.0,
                        "y": 0.0,
                        "z": 0.0
                    },
                    "bboxScale": {
                        "x": 1.0,
                        "y": 1.0,
                        "z": 1.0
                    }
                },
                "estimateSpaceFromSfM": true,
                "estimateSpaceMinObservations": 3,
                "estimateSpaceMinObservationAngle": 10,
                "maxInputPoints": 50000000,
                "maxPoints": 1019000,
                "maxPointsPerVoxel": 1000000,
                "minStep": 2,
                "partitioning": "singleBlock",
                "repartition": "multiResolution",
                "angleFactor": 15.0,
                "simFactor": 15.0,
                "pixSizeMarginInitCoef": 2.0,
                "pixSizeMarginFinalCoef": 4.0,
                "voteMarginFactor": 4.0,
                "contributeMarginFactor": 2.0,
                "simGaussianSizeInit": 10.0,
                "simGaussianSize": 10.0,
                "minAngleThreshold": 1.0,
                "refineFuse": true,
                "helperPointsGridSize": 10,
                "densify": false,
                "densifyNbFront": 1,
                "densifyNbBack": 1,
                "densifyScale": 20.0,
                "nPixelSizeBehind": 4.0,
                "fullWeight": 1.0,
                "voteFilteringForWeaklySupportedSurfaces": true,
                "addLandmarksToTheDensePointCloud": false,
                "invertTetrahedronBasedOnNeighborsNbIterations": 10,
                "minSolidAngleRatio": 0.2,
                "nbSolidAngleFilteringIterations": 2,
                "colorizeOutput": false,
                "addMaskHelperPoints": false,
                "maskHelperPointsWeight": 1.0,
                "maskBorderSize": 4,
                "maxNbConnectedHelperPoints": 50,
                "saveRawDensePointCloud": false,
                "exportDebugTetrahedralization": false,
                "seed": 0,
                "verboseLevel": "info"
            },
            "outputs": {
                "outputMesh": "{cache}/{nodeType}/{uid0}/mesh.obj",
                "output": "{cache}/{nodeType}/{uid0}/densePointCloud.abc"
            }
        },
        "MeshFiltering_1": {
            "nodeType": "MeshFiltering",
            "position": [
                1800,
                0
            ],
            "parallelization": {
                "blockSize": 0,
                "size": 1,
                "split": 1
            },
            "uids": {
                "0": "d5ee6f873c05efbe4fe92292ccfc7039db2cbf29"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "inputMesh": "{Meshing_1.outputMesh}",
                "keepLargestMeshOnly": true,
                "smoothingSubset": "all",
                "smoothingBoundariesNeighbours": 0,
                "smoothingIterations": 5,
                "smoothingLambda": 1.0,
                "filteringSubset": "all",
                "filteringIterations": 1,
                "filterLargeTrianglesFactor": 60.0,
                "filterTrianglesRatio": 0.0,
                "verboseLevel": "info"
            },
            "outputs": {
                "outputMesh": "{cache}/{nodeType}/{uid0}/mesh.obj"
            }
        },
        "Texturing_1": {
            "nodeType": "Texturing",
            "position": [
                2000,
                0
            ],
            "parallelization": {
                "blockSize": 0,
                "size": 1,
                "split": 1
            },
            "uids": {
                "0": "fbdb0b598ddd71a385b2643b16dcbddee94c892a"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{Meshing_1.output}",
                "imagesFolder": "{DepthMap_1.imagesFolder}",
                "inputMesh": "{MeshFiltering_1.outputMesh}",
                "textureSide": 8192,
                "downscale": 2,
                "outputTextureFileType": "png",
                "unwrapMethod": "Basic",
                "useUDIM": true,
                "fillHoles": false,
                "padding": 5,
                "multiBandDownscale": 4,
                "multiBandNbContrib": {
                    "high": 1,
                    "midHigh": 5,
                    "midLow": 10,
                    "low": 0
                },
                "useScore": true,
                "bestScoreThreshold": 0.1,
                "angleHardThreshold": 90.0,
                "processColorspace": "sRGB",
                "correctEV": false,
                "forceVisibleByAllVertices": false,
                "flipNormals": false,
                "visibilityRemappingMethod": "PullPush",
                "subdivisionTargetRatio": 0.8,
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/",
                "outputMesh": "{cache}/{nodeType}/{uid0}/texturedMesh.obj",
                "outputMaterial": "{cache}/{nodeType}/{uid0}/texturedMesh.mtl",
                "outputTextures": "{cache}/{nodeType}/{uid0}/texture_*.{outputTextureFileTypeValue}"
            }
        }
    }
}